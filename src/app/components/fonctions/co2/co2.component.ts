import { Component, OnInit } from '@angular/core';
import {SocketioService} from '../../../services/socketio.service';
import {UrlToYunService} from '../../../services/url-to-yun.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-co2',
  templateUrl: './co2.component.html',
  styleUrls: ['./co2.component.scss']
})
export class CO2Component implements OnInit {

  view: any[] = [1000, 400];
  xAxisLabel: string = 'Temps';
  yAxisLabel: string = 'Taux de CO2 (ppm)';
  timeline: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showXAxisLabel : boolean = true;
  showYAxisLabel : boolean = true;
  legend : boolean = true;
  legendTitle : string = '';
  autoscale : boolean = true;
  colorScheme = {
    domain: ['#a4202f', '#e46c2d', '#396ee5', '#e3e445',
      '#010935', '#341447', '#40151a']
  };

  tauxCO2Actu : number;
  jsonNode : any[] = [];
  results : any[] = [];
  _temp : any[] = [];
  tauxCO2Consigne : number;
  tauxCO2ConsigneForm;


  // results : any[] = [
  //   {
  //     "name": "Basilic",
  //     "series": [
  //       {
  //         "value": 50,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   },
  //
  //   {
  //     "name": "Origan",
  //     "series": [
  //       {
  //         "value": 60,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   }
  // ];

  constructor(private socketServ : SocketioService,
              private yunService : UrlToYunService,
              private formBuilder : FormBuilder)
  {
    this.tauxCO2ConsigneForm = this.formBuilder.group({
      tauxCO2ConsigneNew:''
    })
  }

  ngOnInit(): void {

    this.socketServ.connectionSocket("tauxCO2HubConnect")

    this.socketServ.listen("tauxCO2lAllHub")
      .subscribe(
        (data : any)=> {
          this.jsonNode = data;
          this.JsonConvertFromMongo();
        }
      );


    this.socketServ.listen("tauxCO2HubNew")
      .subscribe(
        (data : any)=> {

          console.log(data)
          this.tauxCO2Actu = parseFloat(data.value)
        }
      )

    this.socketServ.listen("consigneTauxCO2Hub")
      .subscribe(
        (data : any)=> {
          console.log(data);

          this.tauxCO2Consigne = parseFloat(data.value) ;
        }
      )
  }



  JsonConvertFromMongo()
  {
    let i = 0;
    let j = 0;
    let k = 0;
    let _tempName;

    for(let node of this.jsonNode)
    {
      /* part for name of plant */
      if(_tempName != node.box)
      {
        _tempName = node.box;

        this._temp[i] = {
          "name" : node.box,
          "series" : []
        };
        i++
      }
    }

    for(let temp of this._temp)
    {
      for(let node of this.jsonNode)
      {
        if(temp.name == node.box)
        {
          this._temp[j].series[k] = {};
          this._temp[j].series[k].name = new Date(node.date);
          this._temp[j].series[k].value = node.value;
          k++
        }
      }
      j++;
      k=0;
    }

    this.results = [...this._temp];
  }


  allumerVentilo(){
    this.yunService.allumerVentilo();
  }

  changeHumiConsigne(data){
    this.yunService.changeCO2Consigne(data.tauxCO2ConsigneNew);
  }


}
