import { Component, OnInit } from '@angular/core';
import {SocketioService} from '../../../services/socketio.service';
import {UrlToYunService} from '../../../services/url-to-yun.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-p-h',
  templateUrl: './p-h.component.html',
  styleUrls: ['./p-h.component.scss']
})
export class PHComponent implements OnInit {
  view: any[] = [1000, 400];
  xAxisLabel: string = 'Temps';
  yAxisLabel: string = 'pH ';
  timeline: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showXAxisLabel : boolean = true;
  showYAxisLabel : boolean = true;
  legend : boolean = true;
  legendTitle : string = '';
  autoscale : boolean = true;
  colorScheme = {
    domain: ['#5AA454', '#46e422', '#396ee5', '#e3e445',
      '#010935', '#341447', '#40151a']
  };

  pHActu : number;
  jsonNode : any[] = [];
  results : any[] = [];
  _temp : any[] = [];


  // results : any[] = [
  //   {
  //     "name": "Basilic",
  //     "series": [
  //       {
  //         "value": 50,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   },
  //
  //   {
  //     "name": "Origan",
  //     "series": [
  //       {
  //         "value": 60,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   }
  // ];

  constructor(private socketServ : SocketioService,
              private yunService : UrlToYunService)
  {

  }

  ngOnInit(): void {

    this.socketServ.connectionSocket("pHHubConnect")

    this.socketServ.listen("pHHub")
      .subscribe(
        (data : any)=> {
          this.jsonNode = data;
          this.JsonConvertFromMongo();
        }
      );


    this.socketServ.listen("pHHubNew")
      .subscribe(
        (data : any)=> {


          this.pHActu = parseFloat(data.value)
        }
      )



  }



  JsonConvertFromMongo()
  {
    let i = 0;
    let j = 0;
    let k = 0;
    let _tempName;

    for(let node of this.jsonNode)
    {
      /* part for name of plant */
      if(_tempName != node.box)
      {
        _tempName = node.box;

        this._temp[i] = {
          "name" : node.box,
          "series" : []
        };
        i++
      }
    }

    for(let temp of this._temp)
    {
      for(let node of this.jsonNode)
      {
        if(temp.name == node.box)
        {
          this._temp[j].series[k] = {};
          this._temp[j].series[k].name = new Date(node.date);
          this._temp[j].series[k].value = parseFloat(node.value);
          k++
        }
      }
      j++;
      k=0;
    }

    this.results = [...this._temp];
  }


}
