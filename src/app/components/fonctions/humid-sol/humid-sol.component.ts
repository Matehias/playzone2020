import { Component, OnInit } from '@angular/core';
import {SocketioService} from '../../../services/socketio.service';
import {UrlToYunService} from '../../../services/url-to-yun.service';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-humid-sol',
  templateUrl: './humid-sol.component.html',
  styleUrls: ['./humid-sol.component.scss']
})
export class HumidSolComponent implements OnInit {

  view: any[] = [1000, 400];
  xAxisLabel: string = 'Temps';
  yAxisLabel: string = 'Humidité (%)';
  timeline: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showXAxisLabel : boolean = true;
  showYAxisLabel : boolean = true;
  legend : boolean = true;
  legendTitle : string = '';
  autoscale : boolean = true;
  colorScheme = {
    domain: ['#2223a4', '#46e422', '#396ee5', '#e3e445',
      '#010935', '#341447', '#40151a']
  };

  humiActu : number;
  jsonNode : any[] = [];
  results : any[] = [];
  _temp : any[] = [];
  humidConsigne : number;
  humidConsigneForm;


  // results : any[] = [
  //   {
  //     "name": "Basilic",
  //     "series": [
  //       {
  //         "value": 50,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   },
  //
  //   {
  //     "name": "Origan",
  //     "series": [
  //       {
  //         "value": 60,
  //         "name": "2016-09-23T21:55:58.443Z"
  //       },
  //       {
  //         "value": 38,
  //         "name": "2016-09-19T16:30:57.602Z"
  //       }
  //     ]
  //   }
  // ];

  constructor(private socketServ : SocketioService,
              private yunService : UrlToYunService,
              private formBuilder : FormBuilder)
  {
    this.humidConsigneForm = this.formBuilder.group({
      humidConsigneNew:''
    })
  }

  ngOnInit(): void {

    this.socketServ.connectionSocket("HumiSolHubConnect")

    this.socketServ.listen("HumiSolHub")
      .subscribe(
        (data : any)=> {
          this.jsonNode = data;
          this.JsonConvertFromMongo();
        }
      );


    this.socketServ.listen("HumiSolHubNew")
      .subscribe(
        (data : any)=> {


          this.humiActu = parseInt(data.value)
        }
      )

    this.socketServ.listen("consigneHumisSolHub")
      .subscribe(
        (data : any)=> {
          console.log(data);

          this.humidConsigne = parseInt(data.value) ;
        }
      )
  }



  JsonConvertFromMongo()
  {
    let i = 0;
    let j = 0;
    let k = 0;
    let _tempName;

    for(let node of this.jsonNode)
    {
      /* part for name of plant */
      if(_tempName != node.plante)
      {
        _tempName = node.plante;

        this._temp[i] = {
          "name" : node.plante,
          "series" : []
        };
        i++
      }
    }

    for(let temp of this._temp)
    {
      for(let node of this.jsonNode)
      {
        if(temp.name == node.plante)
        {
          this._temp[j].series[k] = {};
          this._temp[j].series[k].name = new Date(node.date);
          this._temp[j].series[k].value = node.value;
          k++
        }
      }
      j++;
      k=0;
    }

    this.results = [...this._temp];
  }


  allumerPompe(){
    this.yunService.allumerPompe();
  }

  changeHumiConsigne(data){
    this.yunService.changeHumiSolConsigne(data.humidConsigneNew);
  }
}
