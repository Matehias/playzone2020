import { Component, OnInit } from '@angular/core';
import {SocketioService} from '../../services/socketio.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  humiSol : number;
  humiAir: number;
  CO2: number;
  temperature : number;
  pH: number;


  constructor(private socketServ : SocketioService ) { }

  ngOnInit(): void {

    this.socketServ.connectionSocket("HumiSolHubConnect")
    this.socketServ.connectionSocket("HumiAirHubConnect")
    this.socketServ.connectionSocket("tauxCO2HubConnect")
    this.socketServ.connectionSocket("TempAirHubConnect")
    this.socketServ.connectionSocket("pHHubConnect")

    this.socketServ.listen("HumiSolHubNew")
      .subscribe(
        (data : any)=> {


          this.humiSol = parseInt(data.value);
        }
      );

    this.socketServ.listen("humidAirHubNew")
      .subscribe(
        (data : any)=> {

          console.log(data)
          this.humiAir = parseFloat(data.value)
        }
      )

    this.socketServ.listen("tauxCO2HubNew")
      .subscribe(
        (data : any)=> {

          console.log(data)
          this.CO2 = parseFloat(data.value)
        }
      )

    this.socketServ.listen("TempAirHubNew")
      .subscribe(
        (data : any)=> {

          console.log(data);

          this.temperature = parseFloat(data.value)
        }
      )

    this.socketServ.listen("pHHubNew")
      .subscribe(
        (data : any)=> {


          this.pH = parseFloat(data.value)
        }
      )
  }

}
