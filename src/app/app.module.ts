import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HumidSolComponent } from './components/fonctions/humid-sol/humid-sol.component';

import { NgxChartsModule } from '@swimlane/ngx-charts'
import {SocketioService} from './services/socketio.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UrlToYunService} from './services/url-to-yun.service';
import {HttpClientModule} from '@angular/common/http';
import { TempAirComponent } from './components/fonctions/temp-air/temp-air.component';
import {ControlContainer, Form, FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HumidAirComponent } from './components/fonctions/humid-air/humid-air.component';
import { CO2Component } from './components/fonctions/co2/co2.component';
import { PHComponent } from './components/fonctions/p-h/p-h.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    HumidSolComponent,
    TempAirComponent,
    HumidAirComponent,
    CO2Component,
    PHComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    SocketioService,
    UrlToYunService,
    FormBuilder,
    ReactiveFormsModule,


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
