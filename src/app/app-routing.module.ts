import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HumidSolComponent } from './components/fonctions/humid-sol/humid-sol.component';
import { HomeComponent } from './components/home/home.component';
import {TempAirComponent} from './components/fonctions/temp-air/temp-air.component';
import {HumidAirComponent} from './components/fonctions/humid-air/humid-air.component';
import {CO2Component} from './components/fonctions/co2/co2.component';
import {PHComponent} from './components/fonctions/p-h/p-h.component';


const routes: Routes = [
  {path : "", component : HomeComponent},
  {path : "humidSol", component : HumidSolComponent},
  {path : "tempAir" , component : TempAirComponent},
  {path : "humidAir", component : HumidAirComponent},
  {path : "co2", component : CO2Component},
  {path : "pH", component : PHComponent}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
