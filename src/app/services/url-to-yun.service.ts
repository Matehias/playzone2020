import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UrlToYunService {

  baseUrl: string = 'http://localhost:3000/';

  constructor(private http: HttpClient) {
  }


  pinPompe: number = 2;
  pinVentilo: number = 4;

  allumerPompe() {
    //ici on va call le node qui va aller caller l'arduino ! pour lui allumer la pin 2 pour allumer la pompe
    var url = this.baseUrl + 'api/arduino/digitalWrite/?pin=' + this.pinPompe + '&status=1';

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }

  allumerVentilo() {
//ici on va call le node qui va aller caller l'arduino ! pour lui allumer la pin 2 pour allumer la pompe
    var url = this.baseUrl + 'api/arduino/digitalWrite/?pin=' + this.pinVentilo + '&status=1';

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }

  changeTempConsigne(tempConsigneNew: number) {
    var url = this.baseUrl + 'api/arduino/seuilTemp/?value=' + tempConsigneNew ;

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }

  changeHumiSolConsigne(humidConsigneNew: number) {
    var url = this.baseUrl + 'api/arduino/seuilHumiSol/?value=' + humidConsigneNew ;

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }

  changeHumiAirConsigne(humidConsigneNew: number) {
    var url = this.baseUrl + 'api/arduino/seuilHumiAir/?value=' + humidConsigneNew ;

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }

  changeCO2Consigne(tauxCO2ConsigneNew: number) {
    var url = this.baseUrl + 'api/arduino/seuilTauxCO2/?value=' + tauxCO2ConsigneNew ;

    this.http.get<any>(url).subscribe(
      (respYun) => {
        console.log(respYun);
      }
    );
  }
}
