import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import {Observable, Subscriber} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  socket: any;

  constructor() {

  }

  connectionSocket(CalledOption : string)
  {
    this.socket = io("http://localhost:3000", {
      query : {
        tutu : CalledOption
      }
    })
  }

  listen(EventName :string){
console.log("listen")
    return new Observable(
      (subscriber) => {
        this.socket.on(EventName,
        (data)=> {subscriber.next(data)});
      }
    )
  }
}
